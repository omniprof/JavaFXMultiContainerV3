package com.kenfogel.javafxmulticontainerv3.view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.kenfogel.fishfxhtml.view.FishFXHTMLController;
import com.kenfogel.fishfxtable.view.FishFXTableController;
import com.kenfogel.fishfxtree.view.FishFXTreeController;
//import com.kenfogel.fishfxwebview.view.FishFXWebViewController;
import com.kenfogel.javafxmulticontainerv3.MainAppFX;
import com.kenfogel.javafxmulticontainerv3.persistence.FishDAO;
import com.kenfogel.javafxmulticontainerv3.persistence.FishDAOImpl;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 * This is the root layout. All of the other layouts are added in code here.
 * This allows us to use the standalone containers with minimal changes.
 * 
 * i18n added
 * 
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class RootLayoutController {

	@FXML
	private AnchorPane upperLeftSplit;

	@FXML
	private AnchorPane upperRightSplit;

	@FXML
	private AnchorPane lowerLeftSplit;

	@FXML
	private AnchorPane lowerRightSplit;
	
    @FXML 
    private ResourceBundle resources;

	private FishDAO fishDAO;
	private FishFXTreeController fishFXTreeController;
	private FishFXTableController fishFXTableController;
	//private FishFXWebViewController fishFXWebViewController;
	private FishFXHTMLController fishFXHTMLController;

	public RootLayoutController() {
		super();
		fishDAO = new FishDAOImpl();
	}

	/**
	 * Here we call upon the methods that load the other containers and then
	 * send the appropriate action command to each container
	 */
	@FXML
	private void initialize() {

		initUpperLeftLayout();
		initUpperRightLayout();
		initLowerLeftLayout();
		initLowerRightLayout();

		// Tell the tree about the table
		setTableControllerToTree();

		try {
			fishFXTreeController.displayTree();
			fishFXTableController.displayTheTable();
			fishFXHTMLController.displayFishAsHTML();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Send the reference to the FishFXTableController to the FishFXTreeController
	 */
	private void setTableControllerToTree() {
		fishFXTreeController.setTableController(fishFXTableController);
	}

	/**
	 * The TreeView Layout
	 */
	private void initUpperLeftLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setResources(resources);
			
			loader.setLocation(MainAppFX.class
					.getResource("/fxml/FishFXTreeLayout.fxml"));
			AnchorPane treeView = (AnchorPane) loader.load();

			// Give the controller the data object.
			fishFXTreeController = loader.getController();
			fishFXTreeController.setFishDAO(fishDAO);

			upperLeftSplit.getChildren().add(treeView);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The TableView Layout
	 */
	private void initUpperRightLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setResources(resources);

			loader.setLocation(MainAppFX.class
					.getResource("/fxml/FishFXTableLayout.fxml"));
			AnchorPane tableView = (AnchorPane) loader.load();

			// Give the controller the data object.
			fishFXTableController = loader.getController();
			fishFXTableController.setFishDAO(fishDAO);

			upperRightSplit.getChildren().add(tableView);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The WebView Layout
	 */
	private void initLowerLeftLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setResources(resources);

			loader.setLocation(MainAppFX.class
					.getResource("/fxml/FishFXWebViewLayout.fxml"));
			AnchorPane webView = (AnchorPane) loader.load();

			// Retrieve the controller if you must send it messages
			//fishFXWebViewController = loader.getController();

			lowerLeftSplit.getChildren().add(webView);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The HTMLEditor Layout
	 */
	private void initLowerRightLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setResources(resources);

			loader.setLocation(MainAppFX.class
					.getResource("/fxml/FishFXHTMLLayout.fxml"));
			AnchorPane htmlView = (AnchorPane) loader.load();

			// Give the controller the data object.
			fishFXHTMLController = loader.getController();
			fishFXHTMLController.setFishDAO(fishDAO);

			lowerRightSplit.getChildren().add(htmlView);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
