package com.kenfogel.javafxmulticontainerv3;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.kenfogel.javafxmulticontainerv3.view.RootLayoutController;

/**
 * In this program we are creating a layout that will contain four other
 * containers. Each of these containers were initially written as stand alone
 * programs. Only methods that are used to provide a reference for one container
 * to be available to another container have been added. No other code was
 * changed.
 * 
 * i18n added
 * 
 * Thanks to Marco Jakob's excellent tutorial at
 * http://code.makery.ch/java/javafx-8-tutorial-intro/ for much of the
 * inspiration.
 * 
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class MainAppFX extends Application {

	private final Logger log = LoggerFactory.getLogger(this.getClass()
			.getName());

	private Stage primaryStage;
	private AnchorPane rootLayout;
	private Locale currentLocale;

	
	public MainAppFX() {
		super();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;

		// Set the application icon using getResourceAsStream.
		this.primaryStage.getIcons().add(
				new Image(MainAppFX.class
						.getResourceAsStream("/images/bluefish_icon.png")));

		initRootLayout();

		this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("Title"));

		Scene scene = new Scene(rootLayout);
		primaryStage.setScene(scene);
		primaryStage.show();
		log.info("Program started");
	}

	/**
	 * Load the layout and controller. When the RootLayoutController runs its
	 * initialize method all the other containers are created.
	 */
	public void initRootLayout() {
		
		Locale locale = Locale.getDefault();
		log.debug("Locale = " + locale);
//		currentLocale = new Locale("en","CA");
		currentLocale = new Locale("fr","CA");

//		Locale currentLocale = Locale.CANADA;
//		Locale currentLocale = Locale.CANADA_FRENCH;
		

		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
			
			
			loader.setLocation(MainAppFX.class
					.getResource("/fxml/RootLayout.fxml"));
			rootLayout = (AnchorPane) loader.load();

			// Retrieve the controller if you must send it messages
			//RootLayoutController rootController = loader.getController();

			// Show the scene containing the root layout.
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Where it all begins
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
		System.exit(0);
	}
}
