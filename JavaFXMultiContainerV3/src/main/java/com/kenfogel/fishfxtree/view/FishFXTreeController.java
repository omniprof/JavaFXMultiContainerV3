package com.kenfogel.fishfxtree.view;

import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;

import com.kenfogel.fishfxtable.view.FishFXTableController;
import com.kenfogel.javafxmulticontainerv3.beans.FishData;
import com.kenfogel.javafxmulticontainerv3.persistence.FishDAO;

/**
 * This controller began its life as part of a standalone display of a container
 * with a menu, tool bar and HTML editor. It is now part of another container.
 * 
 * A method was added to allow the RootLayoutController to pass in a reference
 * to the FishFXTableController
 * 
 * i18n added
 * 
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class FishFXTreeController {

	private FishDAO fishDAO;
	private FishFXTableController fishFXTableController;

	@FXML
	private TreeView<FishData> fishFXTreeView;

	// Resource bundle is injected when controller is loaded
	@FXML
	private ResourceBundle resources;

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// We need a root node for the tree and it must be the same type as all
		// nodes
		FishData rootFish = new FishData();

		// The tree will display common name so we set this for the root
		// Because we are using i18n the root name comes from the resource
		// bundle
		rootFish.setCommonName(resources.getString("Fishies"));

		fishFXTreeView.setRoot(new TreeItem<FishData>(rootFish));

		// This cell factory is used to choose which field in the FihDta object
		// is used for the node name
		fishFXTreeView.setCellFactory((e) -> new TreeCell<FishData>() {
			@Override
			protected void updateItem(FishData item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null) {
					setText(item.getCommonName());
					setGraphic(getTreeItem().getGraphic());
				} else {
					setText("");
					setGraphic(null);
				}
			}
		});
	}

	/**
	 * The RootLayoutController calls this method to provide a reference to the
	 * FishDAO object.
	 * 
	 * @param fishDAO
	 * @throws SQLException
	 */
	public void setFishDAO(FishDAO fishDAO) {
		this.fishDAO = fishDAO;
	}

	/**
	 * The RootLayoutController calls this method to provide a reference to the
	 * FishFXTableController from which it can request a reference to the
	 * TreeView. With theTreeView reference it can change the selection in the
	 * TableView.
	 * 
	 * @param fishFXTableController
	 */
	public void setTableController(FishFXTableController fishFXTableController) {
		this.fishFXTableController = fishFXTableController;
	}

	/**
	 * Build the tree from the database
	 * 
	 * @throws SQLException
	 */
	public void displayTree() throws SQLException {
		// Retrieve the list of fish
		ObservableList<FishData> fishies = fishDAO.findTableAll();

		// Build an item for each fish and add it to the root
		if (fishies != null) {
			for (FishData fd : fishies) {
				TreeItem<FishData> item = new TreeItem<>(fd);
				item.setGraphic(new ImageView(getClass().getResource(
						"/images/fish.png").toExternalForm()));
				fishFXTreeView.getRoot().getChildren().add(item);
			}
		}

		// Open the tree
		fishFXTreeView.getRoot().setExpanded(true);

		// Listen for selection changes and show the fishData details when
		// changed.
		fishFXTreeView
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> showFishDetailsTree(newValue));
	}

	/**
	 * Using the reference to the FishFXTableController it can change the
	 * selected row in the TableView It also displays the FishData object that
	 * corresponds to the selected node.
	 * 
	 * @param fishData
	 */
	private void showFishDetailsTree(TreeItem<FishData> fishData) {

		// Select the row that contains the FishData object from the Tree
		fishFXTableController.getfishDataTable().getSelectionModel()
				.select(fishData.getValue());
		// Get the row number
		int x = fishFXTableController.getfishDataTable().getSelectionModel()
				.getSelectedIndex();
		// Scroll the table so that the row is at the top of the displayed table
		fishFXTableController.getfishDataTable().scrollTo(x);

		System.out.println("showFishDetailsTree\n" + fishData.getValue());
	}
}
