package com.kenfogel.fishfxwebview.view;

import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.web.WebView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This controller began its life as part of a standalone display of a container
 * with a menu, tool bar and HTML editor. It is now part of another container.
 * Nothing was changed when added to the new program.
 * 
 * i18n added
 * 
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class FishFXWebViewController {

	private final Logger log = LoggerFactory.getLogger(this.getClass()
			.getName());

	@FXML
	private WebView fishFXWebView;
	
	// Resource bundle is injected when controller is loaded
    @FXML 
    private ResourceBundle resources;
	
	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded. Not much to do here.
	 */
	@FXML
	private void initialize() {
	       final String html = "example.html";
	        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();
	        log.info("uri= " + uri.toString());
	 
	        // create WebView with specified local content
	        fishFXWebView.getEngine().load(uri.toString());
	}
}
