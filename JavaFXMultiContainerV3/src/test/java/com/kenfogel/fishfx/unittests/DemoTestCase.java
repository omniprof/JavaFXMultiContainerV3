package com.kenfogel.fishfx.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.javafxmulticontainerv3.beans.FishData;
import com.kenfogel.javafxmulticontainerv3.persistence.FishDAO;
import com.kenfogel.javafxmulticontainerv3.persistence.FishDAOImpl;

/**
 * This is a very simple test class. It also creates the database
 * 
 * @author Ken Fogel
 *
 */
public class DemoTestCase {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

	// This is my local MySQL server
	private final String url = "jdbc:mysql://localhost:3306";
	private final String user = "root";
	private final String password = "dawson";

	/**
	 * There should be 200 records in the table
	 * 
	 * @throws SQLException
	 */
	@Ignore
	@Test
	public void testFindAll() throws SQLException {
		FishDAO fd = new FishDAOImpl();
		List<FishData> lfd = fd.findAll();
		assertEquals("testFindAll: ", 200, lfd.size());
	}

	/**
	 * I have selected the record that will have an ID value of 6. I create a
	 * local object with the data I know should be in record 6. The assertEquals
	 * will invoke the equals method of the first object to compare it to the
	 * second. Another good reason for having the equals() method.
	 * 
	 * @throws SQLException
	 */
	@Test
	public void testFindID() throws SQLException {
		FishData fishData1 = new FishData(6, "African Brown Knife", "Xenomystus nigri", "6.0-8.0", "5-19 dH", "72-78F",
				"12 in TL", "Africa", "", "", "Carnivore");
		FishDAO fd = new FishDAOImpl();
		FishData fishData2 = fd.findID(6);
		assertEquals("testFindID for record 6: ", fishData1, fishData2);
	}

	/**
	 * This test, that you should write, should return the number of records
	 * that have a specific diet such as 'Carnivore'. Issue an SQL statement
	 * such as SELECT COUNT(*) FROM FISH WHERE DIET = 'Carnivore' from the MySQL
	 * command prompt. Now you know how many should be found and you can use
	 * that value in the assert statement.
	 */
	@Ignore
	@Test
	public void testFindDiet() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testCreate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	/**
	 * This routine recreates the database for every test. This makes sure that
	 * a destructive test will not interfere with any other test.
	 * 
	 * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
	 * JBoss who helped me out last winter with an issue with Arquillian. Look
	 * up Arquillian to learn what it is.
	 * 
	 * There needs to be at least one test that is not ignored for this to run
	 */
	@Before
	public void seedDatabase() {
		log.info("Seeding");
		final String seedDataScript = loadAsString("createFishMySQL.sql");
		try (Connection connection = DriverManager.getConnection(url, user, password);) {
			for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
				connection.prepareStatement(statement).execute();
			}
		} catch (SQLException e) {
			throw new RuntimeException("Failed seeding database", e);
		}
	}

	/**
	 * The following methods support the seedDatabse method
	 */
	private String loadAsString(final String path) {
		try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
				Scanner scanner = new Scanner(inputStream)) {
			return scanner.useDelimiter("\\A").next();
		} catch (IOException e) {
			throw new RuntimeException("Unable to close input stream.", e);
		}
	}

	private List<String> splitStatements(Reader reader, String statementDelimiter) {
		final BufferedReader bufferedReader = new BufferedReader(reader);
		final StringBuilder sqlStatement = new StringBuilder();
		final List<String> statements = new LinkedList<String>();
		try {
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty() || isComment(line)) {
					continue;
				}
				sqlStatement.append(line);
				if (line.endsWith(statementDelimiter)) {
					statements.add(sqlStatement.toString());
					sqlStatement.setLength(0);
				}
			}
			return statements;
		} catch (IOException e) {
			throw new RuntimeException("Failed parsing sql", e);
		}
	}

	private boolean isComment(final String line) {
		return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
	}
}
